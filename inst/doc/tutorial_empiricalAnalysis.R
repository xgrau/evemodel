## ----evaluate-----------------------------------------------------------------
evalBool <- FALSE

## ----setup, include=FALSE-----------------------------------------------------
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(fig.align='center', fig.height=5, fig.width=6)
# dependencies
library(ape)
library(evemodel)
library(ggplot2)
library(MASS)
library(Rlab)

## ----tree---------------------------------------------------------------------
phy.string <- "(gar:300,(zebrafish:220,((medaka:120,stickleback:120):60,((esox:100,(central_mudminnow:80,olympic_mudminnow:80):20):25,((grayling1:50,(hucho1:35,(salmon1:20,(charr1:10,(rainbow_trout1:5,coho1:5):5):10):15):15):50,(grayling2:50,(hucho2:35,(salmon2:20,(charr2:10,(rainbow_trout2:5,coho2:5):5):10):15):15):50):25):55):40):80);"

tre <- read.tree(text = phy.string)
numIndivs <- 4 # number of desired individuals per species - *needs to be defined by user*
numSpecies <- length(tre$tip.label)
speciesLabels <- rep(tre$tip.label,each=numIndivs)

plot(tre) # plot tree

## ----read in data, warning=FALSE, include=FALSE-------------------------------
#gene.data <- read.csv("https://raw.githubusercontent.com/karzu/data/main/empiricalData.csv", row.names = 1)
path <- textConnection(readLines("https://raw.githubusercontent.com/karzu/data/main/empiricalData.csv"))
gene.data <- read.csv(path, as.is=T, row.names=1)
gene.data <- as.matrix(gene.data)

## ----simulation scheme, include=FALSE-----------------------------------------
scheme <- "https://github.com/karzu/data/blob/main/simulationScheme.png?raw=true"

## ----plot scheme 2, echo=TRUE-------------------------------------------------
thetaShiftBool_full <- 1:Nedge(tre) %in% getEdgesFromMRCA(tre, tips = tre$tip.label[14:length(tre$tip.label)], includeEdgeToMRCA = T)

#pdf(file = "figs/Figure 4A.pdf")
plot(tre,edge.color = ifelse(thetaShiftBool_full,"salmon","black"),edge.width = 2)
#dev.off()

## ----violin plots 986-990, echo=FALSE-----------------------------------------
violinData <- data.frame(theta2 = c(rep("50", numSpecies*numIndivs), rep("60", numSpecies*numIndivs), rep("70", numSpecies*numIndivs), rep("80", numSpecies*numIndivs), rep("90", numSpecies*numIndivs), rep("100", numSpecies*numIndivs)),
                         expression = c(gene.data[950,], gene.data[986,], gene.data[987,], gene.data[988,], gene.data[989,], gene.data[990,]),
                         species = rep(c(rep("black", 13*numIndivs), rep("salmon", 6*numIndivs)), 6))
violinData$theta2 <- as.numeric(as.character(violinData$theta2))

#pdf(file = "figs/Figure 4B.pdf")
p <- ggplot(violinData, aes(x=as.factor(theta2), y=expression, fill=theta2)) + 
  xlab(expression(theta[2])) +
  ylim(40,80) +
  geom_violin(show.legend = FALSE) +
  theme_bw()
p + geom_jitter(shape=16, position=position_jitter(0.2), alpha=0.5, show.legend = FALSE, aes(color=species)) + scale_colour_identity()
#dev.off()


## ----plot scheme 3, echo=TRUE-------------------------------------------------
thetaShiftBool_partial <- 1:Nedge(tre) %in% getEdgesFromMRCA(tre, tips = tre$tip.label[17:length(tre$tip.label)], includeEdgeToMRCA = T)
plot(tre,edge.color = ifelse(thetaShiftBool_partial,"salmon","black"),edge.width = 2)


## ----violin plots 991-995, echo=FALSE-----------------------------------------
violinData <- data.frame(theta2 = c(rep("50", numSpecies*numIndivs), rep("60", numSpecies*numIndivs), rep("70", numSpecies*numIndivs), rep("80", numSpecies*numIndivs), rep("90", numSpecies*numIndivs), rep("100", numSpecies*numIndivs)),
                         expression = c(gene.data[950,], gene.data[991,], gene.data[992,], gene.data[993,], gene.data[994,], gene.data[995,]),
                         species = rep(c(rep("black", 16*numIndivs), rep("salmon", 3*numIndivs)), 6))
violinData$theta2 <- as.numeric(as.character(violinData$theta2))

p <- ggplot(violinData, aes(x=as.factor(theta2), y=expression, fill=theta2)) + 
  xlab(expression(theta[2])) +
  ylim(40,80) +
  geom_violin(show.legend = FALSE)  +
  theme_bw()
p + geom_jitter(shape=16, position=position_jitter(0.2), alpha=0.5, show.legend = FALSE, aes(color=species)) + scale_colour_identity()

## ----null distributions, eval = evalBool--------------------------------------
#  
#  theta <- 50
#  sigma2 <- 0.1
#  alpha <- 0.005
#  betaShared <- 0.1
#  
#  set.seed(3)
#  
#  nullData_1000genes <- simOneTheta(n=1000, tree=tre, colSpecies=speciesLabels, theta = theta, sigma2 = sigma2, alpha = alpha, beta = betaShared)
#  
#  test.nullData_full <- twoThetaTest(tree = tre, gene.data = nullData_1000genes, isTheta2edge = thetaShiftBool_full, colSpecies = speciesLabels) #, cores=4)
#  # null distribution for a theta shift test on the full duplicate clade
#  
#  test.nullData_partial <- twoThetaTest(tree = tre, gene.data = nullData_1000genes, isTheta2edge = thetaShiftBool_partial, colSpecies = speciesLabels) #, cores=4) # null distribution for a theta shift test on the more recent shift
#  
#  test.nullData_betaShared <- betaSharedTest(tree = tre, gene.data = nullData_1000genes, colSpecies = speciesLabels) #, cores =4)

## ----thetaShift test full, eval = evalBool------------------------------------
#  #theta shift test (genes 986-990)
#  test.thetaShift_full <- twoThetaTest(tree = tre, gene.data = gene.data, isTheta2edge = thetaShiftBool_full, colSpecies = speciesLabels) #, cores=4)
#  
#  idx <- c(1:985, 986:990)
#  
#  #pdf(file = "figs/Figure 4C.pdf")
#  qqplot(test.nullData_full$LRT[idx], test.thetaShift_full$LRT[idx], main = "Log-scaled LRTs of empirical data vs \nlog-scaled LRTs of null distribution",
#         xlab = "log(LRTs) of null distribution", ylab = "log(LRTs) of empirical data",
#         pch = 16,
#         col=c(rep("black", 985), rep("salmon", 5), rep("black", 10))[order(test.thetaShift_full$LRT[idx])])
#  abline(a=0, b=1)
#  abline(h = 6.28, v = 6.28, col="#cb4154", lty = 3) # the horizontal and vertical lines are determined by the critical values, which were calculated outside of this tutorial
#  #dev.off()

## ----thetaShift test full plot, echo=FALSE------------------------------------
url1 <- "https://github.com/karzu/data/blob/main/thetaShift_full_plot.png?raw=true"

## ----thetaShift test partial, eval = evalBool---------------------------------
#  #theta shift test (genes 991-995)
#  test.thetaShift_partial <- twoThetaTest(tree = tre, gene.data = gene.data, isTheta2edge = thetaShiftBool_partial, colSpecies = speciesLabels) #, cores=4)
#  
#  idx <- c(1:985, 991:995)
#  
#  #pdf(file = "figs/Figure 4D.pdf")
#  qqplot(test.nullData_partial$LRT[idx], test.thetaShift_partial$LRT[idx], main = "Log-scaled LRTs of empirical data vs \nlog-scaled LRTs of null distribution",
#         xlab = "log(LRTs) of null distribution", ylab = "log(LRTs) of empirical data", pch = 16,
#         col=c(rep("black", 985), rep("salmon", 5), rep("black", 5))[order(test.thetaShift_partial$LRT[idx])])
#  abline(a=0, b=1)
#  abline(h = 3.51, v = 3.51, col="#cb4154", lty = 3) # the horizontal and vertical lines are determined by the critical values, which were calculated outside of this tutorial
#  #dev.off()

## ----thetaShift test partial plot, echo=FALSE---------------------------------
url2 <- "https://github.com/karzu/data/blob/main/thetaShift_partial_plot.png?raw=true"

## ----betaShared test, eval = evalBool-----------------------------------------
#  test.betaShared <- betaSharedTest(tree = tre, gene.data = gene.data, colSpecies = speciesLabels) #, cores =4)
#  
#  idx <- c(1:985, 996:1000)
#  
#  #pdf(file = "figs/Figure 5.pdf")
#  qqplot(test.nullData_betaShared$LRT[idx], test.betaShared$LRT[idx], main = "Log-scaled LRTs of empirical data vs \nlog-scaled LRTs of null distribution",
#         xlab = "log(LRTs) of null distribution", ylab = "log(LRTs) of empirical data", pch = 16,
#         col=c(rep("black", 985), rep("salmon", 5))[order(test.betaShared$LRT[idx])])
#  abline(a=0, b=1)
#  abline(h = 4.69, v = 4.69, col="#cb4154", lty = 3) # the horizontal and vertical lines are determined by the critical values, which were calculated outside of this tutorial
#  #dev.off()

## ----betaShared test plot, echo=FALSE-----------------------------------------
url3 <- "https://github.com/karzu/data/blob/main/betaShared_plot.png?raw=true"

