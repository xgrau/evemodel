## Expression Variance Evolution (EVE) Model - R Package

This R package implements the Expression variance evolution model algorithm described in [Rohlfs and Nielsen 2015](https://doi.org/10.1093/sysbio/syv042). The EVE Model provides a method for joint analyses of quantitative traits, e.g. gene expression, within and between species. The model is based on the Ornstein-Uhlebeck process but also includes a parameter for the ratio of population to evolutionary expression variance. Two specific tests are implemented: `betaSharedTest` - a phylogenetic ANOVA test that can detect genes with increased or decreased ratios of expression divergence to diversity and `twoThetaTest` - a test for lineage specific shifts in expression level. 

### Installation

```
remotes::install_gitlab("sandve-lab/evemodel")
```
